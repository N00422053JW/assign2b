﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Theme="light" %>

<asp:Content runat="server" ContentPlaceHolderID="intro">
    <h2>BASIC</h2>
    <p>Set the background color for the page with standard color, HEX value, RGB value or HSL value. Also, font style and size could be alternet in CSS.</p>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="dataGrid">
        <asp:CodeBox ID="css" runat="server"
        SkinId="CodeBox" code="css_code" owner="Me" codeType="css">
        </asp:CodeBox>
</asp:Content>

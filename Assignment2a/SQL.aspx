﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Theme="dark"%>

<asp:Content runat="server" ContentPlaceHolderID="intro">
    <h2>BETWEEN</h2>
    <p>The BETWEEN operator selects values within a given range. The values can be numbers, text, or dates. 
    The BETWEEN operator is inclusive: begin and end values are included. </p>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="dataGrid">
        <asp:CodeBox ID="sql" runat="server"
        SkinId="CodeBox" code="sql_code" owner="Me" codeType="sql">
        </asp:CodeBox>
</asp:Content>

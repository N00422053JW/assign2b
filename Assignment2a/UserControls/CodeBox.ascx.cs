﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2a
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string CodeType
        {
            get { return (string)ViewState["CodeType"]; }
            set { ViewState["CodeType"] = value; }
        }

        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();

            //First column is the line number of the code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> sql_code = new List<string>(new string[]{
                "SELECT column_name(s)",
                "FROM table_name",
                "WHERE column_name BETWEEN value1 AND value_2;",
         
            });
            List<string> js_code = new List<string>(new string[]{
                "var myObjects = {",
                "~~~name : ^myName^,",
                "~~~studentnumber : ^N00123456^,",
                "~~~class : ^section A^",
                "~~~year : ^2018^,",
                "~~~semester : ^fall^",
                "}"
            });
            List<string> css_code = new List<string>(new string[]{
                "body {",
                "~~background-color: red;",
                "}",
                " ",
                "p { ",
                "~~font-family: Arial;",
                "~~font-size: 14px;",
                "}"
            });
            List<string> asp_code = new List<string>(new string[]{
                "<asp:DropDownList id=^ddlSample^ runat=^server^",
                "style=width: ^200px^>",
                "~~<asp:ListItem>Option 1</asp:ListItem>",
                "~~<asp:ListItem>Option 2</asp:ListItem>",
                "~~<asp:ListItem>Option 3</asp:ListItem>",
                "~~<asp:ListItem>Option 4</asp:ListItem>",
                "</asp:DownDownList>"
            });

            List<string> showCode = new List<string>();
            if (CodeType == "sql" ){
                showCode = sql_code;
            }else if (CodeType == "js"){
                showCode = js_code;
            }else if (CodeType == "css"){
                showCode = css_code;
            }else if(CodeType == "asp"){
                showCode = asp_code;
            }

            int i = 1;
            foreach (string code_line in showCode)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                formatted_code = formatted_code.Replace("^", "&quot;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code.DataSource = ds;

            /*Some formatting in the codebehind*/

            Code.DataBind();
        }

    }

}
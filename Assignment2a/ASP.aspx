﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Theme="dark" %>

<asp:Content runat="server" ContentPlaceHolderID="intro">
    <h2>LISTITEM CLASS</h2>
    <p>A ListItem control represents an individual data item within a data-bound list control, such as a ListBox or a RadioButtonList control. 
    The most common method is by placing text in the inner HTML content.
    The inner HTML content is the text between the opening and closing tags of the ListItem control. </p>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="dataGrid">
        <asp:CodeBox ID="asp" runat="server"
        SkinId="CodeBox" code="asp_code" owner="Me" codeType="asp">
        </asp:CodeBox>
</asp:Content>
